Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xawtv
Source: http://git.linuxtv.org/xawtv3.git

Files: *
Copyright: 2011 Mauro Carvalho Chehab
 1997-2005 Gerd Knorr
 2002 Malte Starostik
 1996,1997 Marcus Metzler
 1991-1998 Jamie Zawinski
 1991 Andreas Stolcke
 1990 Solbourne Computer Inc.
Comment: Also includes code written by:
 .
 Heiko Eissfeldt
 Devin Heitmueller
 Jaroslav Kysela
 .
 without identifiable copyright years
 from the previous d/copyright file.
License: GPL-2+

Files: contrib/motv.metainfo.xml
 contrib/mtt.metainfo.xml
 contrib/xawtv.appdata.xml
Copyright: Gerd Knorr <kraxel@bytesex.org>
License: CC0-1.0
 On Debian systems the full text of the CC0-1.0 license can be found in
 '/usr/share/common-licenses/CC0-1.0'

Files: debian/*
Copyright: 2000-2005 Gerd Knorr <kraxel@bytesex.org>
 2006 Matej Vela <vela@debian.org>
 2007 Krzysztof Burghardt <krzysztof@burghardt.pl>
 2012-2018 Maximiliano Curia <maxy@debian.org>
 2019 Dmitry Eremin-Solenikov <dbaryshkov@gmail.com>
 2020 Jeremy Sowden <jeremy@azazel.net>
 2020 Vasyl Gello <vasek.gello@gmail.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
